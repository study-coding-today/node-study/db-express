let express = require('express');
let server = express();
let mysql2 = require('mysql2');
// DB 와 연결할 소켓 생성
let dbPool = mysql2.createPool({
    host: 'localhost',
    user: 'jgh',
    password: '123456',
    database: 'board',
    connectionLimit: 30,
    waitForConnections: true
});

// 웹브라우저에서 요청시 보내주면 되는 파일 디렉토리
server.use(express.static('./statics'));
// POST 방식 요청에 대해 body 데이터가 존재할 경우 파싱해서 req.body 객체를 생성해주는 미들웨어
server.use(express.urlencoded());

// 게시물 목록 화면
server.get('/list', (req, res, next) => {
    // 화면에 최근쓴 글의 내림차순으로 출력하기 위해 DB 에서 가져오기
    dbPool.query('SELECT * FROM post ORDER BY no DESC;', (err, result) => {
        let html = '<html><head><meta charset="UTF-8"><title>Express DB 게시판</title></head><body>';
        html += '<table class="postList" width="100%" cellpadding="0" cellspacing="0">';
        html += '<tr><th>No</th><th>제목</th><th>작성자</th><th>작성일</th><th>조회수</th><th>종아요</th></tr>';
        // err 를 확인하고 오류 없으면 result 를 순회하면서 게시물 목록 만든다
        if (err) {
            console.log('DB 입출력에 오류가 발생했습니다.');
            console.log(err);
        } else {
            for (let i = 0; i < result.length; i++) {
                html += '<tr><td>'+ (result.length-i) +'</td>';
                // 선택한 게시글 상세보기위한 글 번호 넘기기
                html += '<td><a href="/view/'+ result[i].no +'">'+ result[i].subject +'</a></td>';
                html += '<td>'+ result[i].author +'</td>';
                html += '<td>'+ result[i].date +'</td>';
                html += '<td>'+ result[i].views +'</td>';
                html += '<td>'+ result[i].likes +'</td></tr>';
            }
            html += '</table><a href="write.html"><input type="button" value="글쓰기"></a></body></html>';
        }
        res.send(html);
    });
});

// 새 게시물 쓰기 및 기존 게시물 수정 처리
server.post('/write', (req, res, next) => {
    // 새로운 게시물 작성할 때와 기존 게시물 수정하기 위해 게시물번호가 있는 data 인지 확인
    if (req.body.no == undefined) {
        // 게시물 마지막 번호 가져오는 쿼리
        dbPool.query('SELECT * FROM post ORDER BY no DESC LIMIT 1;', (err, result) => {
            if (err) {
                console.log('DB 입출력에 오류가 발생했습니다.');
                console.log(err);
            }
            let len = (result[0].no) + 1;
            // 작성한 게시글 DB 에 넣기
            dbPool.query('INSERT INTO post SET no=?, subject=?, content=?, author=?, date=?, views=?, likes=?',
                [len, req.body.subject, req.body.content, req.body.author, new Date(), 0, 0]);
            res.redirect('/list');
        });
    } else {
        // 기존 게시물을 수정하기 위해 DB에 저장
        dbPool.query('UPDATE post SET subject=?, content=?, date=? WHERE no=?', [req.body.subject, req.body.content, new Date(), req.body.no]);
        res.redirect('/view/' + req.body.no);
    }
});

// 게시물 상세보기 화면
server.get('/view/:idx', (req, res, next) => {
    dbPool.query('SELECT * FROM post WHERE no='+ req.params.idx +';', (err, result) => {
        if (err) {
            console.log('DB 입출력에 오류가 발생했습니다.');
            console.log(err);
        }
        // 조회수 증가
        let view = result[0].views + 1;
        // 증가된 조회수 DB에 넣기
        dbPool.query('UPDATE post SET views=? WHERE no=?', [view, req.params.idx], () => {
            // 클릭된 게시글 DB 에서 가져오기
            dbPool.query('SELECT * FROM post WHERE no=?', [req.params.idx], (err, result) => {
                // err 를 확인하고 오류 없으면 result 데이터를 html 형태로 만든다.
                if (err) {
                    console.log('DB 입출력에 오류가 발생했습니다.');
                    console.log(err);
                }
                let html = '<html><head><title>게시글 상세화면</title><meta charset="utf-8">';
                html += '<style>';
                html += '  .article { border:1px solid #cccccc; border-width: 1px 0px 0px 1px; }';
                html += '  .article th, .article td { border:1px solid #cccccc; border-width: 0px 1px 1px 0px; }';
                html += '</style></head><body>';
                html += '<table style="width: 100%" class="article">';
                html += '<tr><th>제목</th><td colspan="7">'+result[0].subject+'</td></tr>';
                html += '<tr><th>작성자</th><td>'+result[0].author+ '</td>';
                html += '<th>작성일시</th><td>'+result[0].date+'</td>';
                html += '<th>조회수</th><td>'+result[0].views+'</td>';
                html += '<th>좋아요</th><td><a href="/like/'+ req.params.idx + '">'+result[0].likes+'</a></td></tr>';
                html += '<tr><th>내용</th><td colspan="7" style="height:400px;vertical-align:top;">'+result[0].content+'</td></tr>';
                html += '</table>';
                // 클릭된 게시글에 딸린 댓글 DB 에서 가져오기.
                dbPool.query('SELECT * FROM comment WHERE postnum=?', [req.params.idx], (err, result) => {
                    if (err) {
                        console.log('DB 입출력에 오류가 발생했습니다.');
                        console.log(err);
                    }
                    // DB 에 댓글이 없으면 댓글 없이 출력
                    if (result[0] == undefined) {
                        html += '</table>';
                    } else { // DB 에 댓글이 있을때 가져와 출력
                        for (let i = 0; i < result.length; i++) {
                            html += '<table style="width: 100%" class="article">';
                            html += '<tr><th>작성자</th><td>' + result[i].author + '</td>';
                            html += '<th>작성일시</th><td>' + result[i].date + '</td>';
                            html += '<td><a href="/commentEdit/'+ result[i].no +'"><input type="button" value="수정"></a></td>';
                            html += '<td><a href="/commentDel/'+ result[i].no +'"><input type="button" value="삭제"></a></td>';
                            html += '<tr><th>댓글</th><td colspan="6" style="height:50px; vertical-align:top;">' + result[i].content + '</td></tr>';
                            html += '</table>';
                        }
                    }
                    // 댓글 작성할 폼 화면에 출력
                    html += '<form action="/comment/'+ req.params.idx +'" method="get">';
                    html += '<table style="width: 100%" class="article">';
                    html += '<tr><th>작성자</th><td colspan="7"><input type="text" name="author"></td>';
                    html += '<tr><th>댓글</th><td colspan="6" style="height:50px; vertical-align:top;">';
                    html += '<textarea name="content" style="width: 680px; height: 100px;"></textarea>';
                    html += '</td><td><input type="submit" value="댓글저장"></td></tr>';
                    html += '</table>';
                    html += '<br><a href="/edit/'+ req.params.idx +'"><input type="button" value="글수정"></a>';
                    html += '<br><a href="/del/'+ req.params.idx +'"><input type="button" value="글삭제"></a>';
                    html += '<br><a href="/list"><input type="button" value="글목록"></a></body></html>';
                    res.send(html);
                });
            });
        });
    });
});

// 댓글 작성시 DB 저장하고 재출력 호출하기
server.get('/comment/:idx', (req, res, next) => {
    console.dir(req);
    dbPool.query('SELECT * FROM comment ORDER BY no DESC LIMIT 1;', (err, result) => {
        if (err) {
            console.log('DB 입출력에 오류가 발생했습니다.');
            console.log(err);
        }
        // 댓글번호증가
        let len = (result[0].no) + 1;
        // 작성된 댓글 DB 에 넣기
        dbPool.query('INSERT INTO comment SET no=?, content=?, author=?, date=?, postnum=?',
            [len, req.query.content, req.query.author, new Date(), req.params.idx]);
        res.redirect('/view/'+ req.params.idx);
    })
});

// 좋아요 클릭하여 횟수 처리
server.get('/like/:idx', (req, res, next) => {
    // 좋아요 클릭한 게시글 DB 에서 가져오기
    dbPool.query('SELECT * FROM post WHERE no=' + req.params.idx, (err, result) => {
        if (err) {
            console.log('DB 입출력에 오류가 발생했습니다.');
            console.log(err);
        }
        // 좋아요 횟수 증가하기
        let like = result[0].likes + 1;
        // 증가된 좋아요 DB 에 저장하고 재출력하기
        dbPool.query('UPDATE post SET likes=? WHERE no=?', [like, req.params.idx]);
        res.redirect('/view/'+ req.params.idx);
    });
});

// 글삭제 처리하기 ( 댓글이 딸려 있으면 삭제안함 )
server.get('/del/:idx', (req, res, next) => {
    dbPool.query('SELECT * FROM comment WHERE postnum=' + req.params.idx, (err, result) => {
        // 게시글에 딸린 댓글이 없으면 게시글 삭제하고 목록출력
        if (result[0] == undefined) {
            dbPool.query('DELETE FROM post WHERE no='+ req.params.idx);
            res.redirect('/list');
        } else {
            // 게시글에 딸린 댓글이 있으면 경고창 띄우고 다시 상세화면출력
            dbPool.query('SELECT * FROM post WHERE no='+ req.params.idx +';', (err, result) => {
                if (err) {
                    console.log('DB 입출력에 오류가 발생했습니다.');
                    console.log(err);
                }
                // 조회수 증가
                let view = result[0].views + 1;
                // 증가된 조회수 DB에 넣기
                dbPool.query('UPDATE post SET views=? WHERE no=?', [view, req.params.idx], () => {
                    // 클릭된 게시글 DB 에서 가져오기
                    dbPool.query('SELECT * FROM post WHERE no=?', [req.params.idx], (err, result) => {
                        // err 를 확인하고 오류 없으면 result 데이터를 html 형태로 만든다.
                        if (err) {
                            console.log('DB 입출력에 오류가 발생했습니다.');
                            console.log(err);
                        }
                        let html = '<html><head><title>게시글 상세화면</title><meta charset="utf-8">';
                        html += '<style>';
                        html += '  .article { border:1px solid #cccccc; border-width: 1px 0px 0px 1px; }';
                        html += '  .article th, .article td { border:1px solid #cccccc; border-width: 0px 1px 1px 0px; }';
                        html += '</style><script>alert("댓글이 딸려 있으면 삭제할 수 없습니다.");</script></head><body>';
                        html += '<table style="width: 100%" class="article">';
                        html += '<tr><th>제목</th><td colspan="7">'+result[0].subject+'</td></tr>';
                        html += '<tr><th>작성자</th><td>'+result[0].author+ '</td>';
                        html += '<th>작성일시</th><td>'+result[0].date+'</td>';
                        html += '<th>조회수</th><td>'+result[0].views+'</td>';
                        html += '<th>좋아요</th><td><a href="/like/'+ req.params.idx + '">'+result[0].likes+'</a></td></tr>';
                        html += '<tr><th>내용</th><td colspan="7" style="height:400px;vertical-align:top;">'+result[0].content+'</td></tr>';
                        html += '</table>';
                        // 클릭된 게시글에 딸린 댓글 DB 에서 가져오기.
                        dbPool.query('SELECT * FROM comment WHERE postnum=?', [req.params.idx], (err, result) => {
                            if (err) {
                                console.log('DB 입출력에 오류가 발생했습니다.');
                                console.log(err);
                            }
                            // DB 에 댓글이 없으면 댓글 없이 출력
                            if (result[0] == undefined) {
                                html += '</table>';
                            } else { // DB 에 댓글이 있을때 가져와 출력
                                for (let i = 0; i < result.length; i++) {
                                    html += '<table style="width: 100%" class="article">';
                                    html += '<tr><th>작성자</th><td>' + result[i].author + '</td>';
                                    html += '<th>작성일시</th><td>' + result[i].date + '</td>';
                                    html += '<td>수정</td><td>삭제</td>';
                                    html += '<tr><th>댓글</th><td colspan="6" style="height:50px; vertical-align:top;">' + result[i].content + '</td></tr>';
                                    html += '</table>';
                                }
                            }
                            // 댓글 작성할 폼 화면에 출력
                            html += '<form action="/comment/'+ req.params.idx +'" method="get">';
                            html += '<table style="width: 100%" class="article">';
                            html += '<tr><th>작성자</th><td colspan="7"><input type="text" name="author"></td>';
                            html += '<tr><th>댓글</th><td colspan="6" style="height:50px; vertical-align:top;">';
                            html += '<textarea name="content" style="width: 680px; height: 100px;"></textarea>';
                            html += '</td><td><input type="submit" value="댓글저장"></td></tr>';
                            html += '</table>';
                            html += '<br><a href="/del/'+ req.params.idx +'"><input type="button" value="글삭제"></a>';
                            html += '<br><a href="/list">목록</a></body></html>';
                            res.send(html);
                        });
                    });
                });
            });
        }
    });
    // 게시글 및 딸린 댓글 함께 삭제할 때
    /*dbPool.query('DELETE FROM post WHERE no='+ req.params.idx);
    dbPool.query('DELETE FROM comment WHERE postnum='+ req.params.idx);
    res.redirect('/list');*/
});

// 게시글 수정
server.get('/edit/:idx', (req, res, next) => {
    dbPool.query('SELECT * FROM post WHERE no=?', [req.params.idx], (err, result) => {
        // err 를 확인하고 오류 없으면 result 데이터를 html 형태로 만든다.
        if (err) {
            console.log('DB 입출력에 오류가 발생했습니다.');
            console.log(err);
        }
        let html = '<html><head><title>게시글 상세화면</title><meta charset="utf-8">';
        html += '<style>';
        html += '  .article { border:1px solid #cccccc; border-width: 1px 0px 0px 1px; }';
        html += '  .article th, .article td { border:1px solid #cccccc; border-width: 0px 1px 1px 0px; }';
        html += '</style></head><body>';
        html += '<form action="/write" method="post">';
        html += '<table style="width: 100%" class="article"><input type="hidden" name="no" value="'+ result[0].no +'">';
        html += '<tr><th>제목</th><td colspan="7"><input type="text" name="subject" value="'+ result[0].subject+'"></td></tr>';
        html += '<tr><th>작성자</th><td>'+ result[0].author +'</td>';
        html += '<th>작성일시</th><td><input type="text" name="date" value="'+ new Date() +'"></td>';
        html += '<th>조회수</th><td>'+result[0].views+'</td>';
        html += '<th>좋아요</th><td>'+result[0].likes+'</td></tr>';
        html += '<tr><th>내용</th><td colspan="7" style="height:400px;vertical-align:top;">';
        html += '<textarea name="content" style="height: 400px; width: 700px;">'+ result[0].content +'</textarea></td></tr>';
        html += '</table>';
        // 클릭된 게시글에 딸린 댓글 DB 에서 가져오기.
        dbPool.query('SELECT * FROM comment WHERE postnum=?', [req.params.idx], (err, result) => {
            if (err) {
                console.log('DB 입출력에 오류가 발생했습니다.');
                console.log(err);
            }
            // DB 에 댓글이 없으면 댓글 없이 출력
            if (result[0] == undefined) {
                html += '</table>';
            } else { // DB 에 댓글이 있을때 가져와 출력
                for (let i = 0; i < result.length; i++) {
                    html += '<table style="width: 100%" class="article">';
                    html += '<tr><th>작성자</th><td>' + result[i].author + '</td>';
                    html += '<th>작성일시</th><td>' + result[i].date + '</td>';
                    html += '<td><input type="button" value="수정"></td><td><input type="button" value="삭제"></td>';
                    html += '<tr><th>댓글</th><td colspan="6" style="height:50px; vertical-align:top;">' + result[i].content + '</td></tr>';
                    html += '</table>';
                }
            }
            html += '<br><a href="/view/'+ req.params.idx +'"><input type="button" value="취소"></a>';
            html += '<br><input type="submit" value="글저장">';
            html += '</form>';
            html += '<a href="/list"><input type="button" value="글목록"></a></body></html>';
            res.send(html);
        });
    });
});

// 댓글수정
server.get('/commentEdit/:idx', (req, res, next) => {
    // 게시글 번호를 확인하기 위해 DB 검색
    dbPool.query('SELECT * FROM comment WHERE no=' + req.params.idx, (err, result) => {
        if (err) {
            console.log('DB 입출력에 오류가 발생했습니다.');
            console.log(err);
        }
        // 해당 댓글이 달린 게시글 가져오기
        dbPool.query('SELECT * FROM post WHERE no=' + result[0].postnum, (errPost, resultPost) => {
            if (errPost) {
                console.log('DB 입출력에 오류가 발생했습니다.');
                console.log(errPost);
            }
            let html = '<html><head><title>게시글 상세화면</title><meta charset="utf-8">';
            html += '<style>';
            html += '  .article { border:1px solid #cccccc; border-width: 1px 0px 0px 1px; }';
            html += '  .article th, .article td { border:1px solid #cccccc; border-width: 0px 1px 1px 0px; }';
            html += '</style></head><body>';
            html += '<table style="width: 100%" class="article">';
            html += '<tr><th>제목</th><td colspan="7">'+resultPost[0].subject+'</td></tr>';
            html += '<tr><th>작성자</th><td>'+resultPost[0].author+ '</td>';
            html += '<th>작성일시</th><td>'+resultPost[0].date+'</td>';
            html += '<th>조회수</th><td>'+resultPost[0].views+'</td>';
            html += '<th>좋아요</th><td>'+resultPost[0].likes+'</a></td></tr>';
            html += '<tr><th>내용</th><td colspan="7" style="height:400px;vertical-align:top;">'+resultPost[0].content+'</td></tr>';
            html += '</table>';
            html += '<form action="/comment/:'+ req.params.idx + '" method="get">';

            dbPool.query('SELECT * FROM comment WHERE postnum=?', [result[0].postnum], (err1, result1) => {
                if (err1) {
                    console.log('DB 입출력에 오류가 발생했습니다.');
                    console.log(err1);
                }
                // 댓글이 한개이상 인지 확인
                if (result1.length == 1) {
                    // 선택된 댓글 하나만 있을 때 수정할 수 있도록 변경
                    html += '<table style="width: 100%" class="article">';
                    html += '<tr><th>작성자</th><td>' + result1[0].author + '</td>';
                    html += '<th>작성일시</th><td><input type="text" name="date" value="'+ new Date() +'"></td>';
                    html += '<td>수정</td><td></a> 삭제</td>';
                    html += '<tr><th>댓글</th><td colspan="6" style="height:50px; vertical-align:top;">';
                    html += '<textarea name="content" style="width: 680px; height: 100px;"></textarea>';
                    html += '</td><td><input type="submit" value="댓글저장"></td></tr>';
                    html += '</table>';
                } else {
                    // 선택된 댓글외에 다른 댓글이 있을때
                    for (let i = 0; i < result1.length; i++) {
                        // 수정하려는 댓글만 수정할 수 있도록 변경
                        if ( req.params.idx == result1[i].no ) {
                            html += '<table style="width: 100%" class="article">';
                            html += '<tr><th>작성자</th><td>' + result1[i].author + '</td>';
                            html += '<th>작성일시</th><td><input type="text" name="date" value="'+ new Date() +'"></td>';
                            html += '<td>수정</td><td></a> 삭제</td>';
                            html += '<tr><th>댓글</th><td colspan="6" style="height:50px; vertical-align:top;">';
                            html += '<textarea name="content" style="width: 680px; height: 100px;"></textarea>';
                            html += '</td><td><input type="submit" value="댓글저장"></td></tr>';
                            html += '</table>';
                        } else {
                            // 수정하지 않는 딸린 댓글 출력하기
                            html += '<table style="width: 100%" class="article">';
                            html += '<tr><th>작성자</th><td>' + result1[i].author + '</td>';
                            html += '<th>작성일시</th><td>' + result1[i].date + '</td>';
                            html += '<td><input type="button" value="수정"></td><td><input type="button" value="삭제"></td>';
                            html += '<tr><th>댓글</th><td colspan="6" style="height:50px; vertical-align:top;">' + result1[i].content + '</td></tr>';
                            html += '</table>';
                        }
                    }
                }
                html += '</form>';
                html += '<br><a href="/edit/'+ req.params.idx +'"><input type="button" value="글수정"></a>';
                html += '<br><a href="/del/'+ req.params.idx +'"><input type="button" value="글삭제"></a>';
                html += '<br><a href="/list"><input type="button" value="글목록"></a></body></html>';
                res.send(html);
            });
        });
    });
});

// 댓글삭제
server.get('/commentDel/:idx', (req, res, next) => {
    dbPool.query('SELECT * FROM comment WHERE no=' + req.params.idx, (err, result) => {
        if (err) {
            console.log('DB 입출력에 오류가 발생했습니다.');
            console.log(err);
        }
        dbPool.query('DELETE FROM comment WHERE no=' + req.params.idx);
        res.redirect('/view/' + result[0].postnum);
    });
});

// 대댓글 만들기
// ....조횟수 증가.....??

server.listen(3000, () => {
    console.log('서버의 3000번 port 가 준비되었습니다.');
});
